console.log("Hello World");

const getCube = 2**3
console.log("The cube of 2 is "+getCube);


const address = ["258 Washington Ave", "NW, California", "90011"];
const [numberAve, state, zipCode] = address;
console.log(`I live at ${numberAve} ${state} ${zipCode}`);

const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
const {name, type, weight, measurement} = animal
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`)


const numbers = [1, 2, 3, 4, 5];
numbers.forEach((numbers)=>{
	console.log(`${numbers}`);
})



let sum = numbers.reduce((a, b) =>{
	return a+b;
});

console.log(sum)



class dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
console.log(myDog);